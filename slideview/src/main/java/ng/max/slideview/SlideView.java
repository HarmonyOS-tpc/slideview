package ng.max.slideview;

import ng.max.slideview.utils.ResUtil;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class SlideView extends DependentLayout implements SliderBar.OnSliderBarChangeListener {


    private SliderBar slider;
    private Text sliderText;

    private Element slideBackground;
    private Element buttonImage;
    private Element buttonImageDisabled;

    private int buttonBackgroundColor;
    private int slideBackgroundColor;

    private boolean animateSlideText = true;
    private boolean reversed = false;


    public SlideView(Context context) {
        this(context, null, null);
    }

    public SlideView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SlideView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    private void init(Context context, AttrSet attrSet) {
        LayoutScatter scatter = LayoutScatter.getInstance(context);
        Component component = scatter.parse(ResourceTable.Layout_sv_slide_view, null, false);
        super.addComponent(component);


        setBackground(new ShapeElement(context, ResourceTable.Graphic_sv_view_bg));
        slider = (SliderBar) findComponentById(ResourceTable.Id_sv_slider);
        sliderText = (Text) findComponentById(ResourceTable.Id_sv_text);
        slider.setOnSliderBarChangeListener(this);
        slideBackground = getBackgroundElement();
        slider.setMustInclude(true);

        Element defaultButton =  ResUtil.getDrawable(context, ResourceTable.Media_sv_ic_chevron_double_right);

        if (attrSet != null){
            attrSet.getAttr("sv_animateSlideText").ifPresent(attr -> animateSlideText = attr.getBoolValue());
            attrSet.getAttr("sv_reverseSlide").ifPresent(attr -> reversed = attr.getBoolValue());
            attrSet.getAttr("sv_slideTextSize").ifPresent(attr -> sliderText.setTextSize(attr.getIntegerValue(), Text.TextSizeType.PX));
            attrSet.getAttr("sv_slideText").ifPresent(attr -> setText(attr.getStringValue()));
            attrSet.getAttr("sv_slideTextColor").ifPresent(attr -> setTextColor(attr.getColorValue().getValue()));

            if (attrSet.getAttr("sv_buttonImage").isPresent()){
                setButtonImage(attrSet.getAttr("sv_buttonImage").get().getElement());
            }else {
                setButtonImage(defaultButton);
            }
            if (attrSet.getAttr("sv_buttonImageDisabled").isPresent()){
                setButtonImageDisabled(attrSet.getAttr("sv_buttonImageDisabled").get().getElement());
            }else {
                setButtonImageDisabled(defaultButton);
            }

            attrSet.getAttr("sv_buttonBackgroundColor").ifPresent(attr -> setButtonBackgroundColor(attr.getColorValue().getValue()));
            attrSet.getAttr("sv_slideBackgroundColor").ifPresent(attr -> setSlideBackgroundColor(attr.getColorValue().getValue()));
            attrSet.getAttr("sv_strokeColor").ifPresent(attr -> Util.setDrawableStroke(slideBackground, attr.getColorValue().getValue()));
        }else {
            setButtonImage(defaultButton);
            setButtonImageDisabled(defaultButton);
        }

        if (reversed) {
            setReversed(reversed);
        }

    }

    public void setOnSlideCompleteListener(OnSlideCompleteListener listener) {
        slider.setOnSlideCompleteListenerInternal(listener, this);
    }


    public interface OnSlideCompleteListener {
        void onSlideComplete(SlideView slideView);
    }

    public void setTextColor(int color) {
        sliderText.setTextColor(new Color(color));
    }

    public void setText(String text) {
        sliderText.setText(text);
    }

    public void setTextSize(int textSize) {
        sliderText.setTextSize(textSize);
    }

    public Text getTextView() {
        return sliderText;
    }

    public SliderBar getSlider() {
        return slider;
    }

    public void setButtonImage(Element image) {
        buttonImage = image;

        slider.setThumbElement(image);
    }



    public void setButtonImageDisabled(Element image) {
        buttonImageDisabled = image;
    }

    public void setButtonBackgroundColor(int color) {
        if (buttonBackgroundColor == color){
            return;
        }
        buttonBackgroundColor = color;
        slider.setBackgroundColor(color);
    }

    public void setSlideBackgroundColor(int color) {
        if (slideBackgroundColor == color){
            return;
        }
        slideBackgroundColor = color;
        Util.setDrawableColor(slideBackground, color);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (int i = 0; i < getChildCount(); i++) {
            getComponentAt(i).setEnabled(enabled);
        }

        slider.setThumbElement(enabled ? buttonImage :
                buttonImageDisabled == null ? buttonImage : buttonImageDisabled);

        setButtonBackgroundColor(enabled ? buttonBackgroundColor : 0xFFAEAEAE);
        setSlideBackgroundColor(enabled ? slideBackgroundColor : 0xFF757575);
    }
    public void setStokeColor(int color){
        Util.setDrawableStroke(slideBackground, color);
    }

    public void setReversed(boolean reversed) {
        this.reversed = reversed;
        LayoutConfig layoutConfig = (LayoutConfig) sliderText.getLayoutConfig();
        if (reversed){
            layoutConfig.addRule(LayoutConfig.ALIGN_PARENT_END,0);
            layoutConfig.addRule(LayoutConfig.ALIGN_PARENT_START);
            slider.setLayoutDirection(LayoutDirection.RTL);
        }else {
            layoutConfig.addRule(LayoutConfig.ALIGN_PARENT_START,0);
            layoutConfig.addRule(LayoutConfig.ALIGN_PARENT_END);
            slider.setLayoutDirection(LayoutDirection.LTR);
        }
        sliderText.setLayoutConfig(layoutConfig);
    }

    public boolean isReversed() {
        return reversed;
    }

    @Override
    public void onProgressChanged(SliderBar sliderBar, int progress, boolean fromUser) {
        if (animateSlideText) {
            sliderText.setAlpha(1 - (progress / 100f));
        }
    }

    @Override
    public void onStartTrackingTouch(SliderBar sliderBar) {

    }

    @Override
    public void onStopTrackingTouch(SliderBar sliderBar) {

    }

}
